@extends('layouts.master')

@section('content')
    <!-- hero section starts -->
    <section id="home" class="dtr-section dtr-section-with-bg dtr-hero-section-top-padding bg-blue"
        style="background-image: url({{ asset('assets/images/white-shape-bg.png') }});">

        <!-- wrapping div for background bottom curve stripes image - easy to change color - no need to edit image - refer help doc -->
        <div class="dtr-bottom-shape-img" style="background-image: url({{ asset('assets/images/hero-bottom.svg') }});">
            <div class="container">

                <!--===== row 1 starts =====-->
                <div class="row">

                    <!-- column 1 starts -->
                    <div class="col-12 col-md-6">

                        <!-- intro text -->
                        <h1 class="color-white">Riset dan Konsultan</h1>
                        <p class="color-white-muted">Parameter Politik Indonesia (PPI) adalah lembaga terpadu
                            yang bergerak di bidang survei sosial-ekonomi dan political marketing. Lembaga ini
                            ditujukan untuk memberi layanan survei seputar masalah kemasyarakatan, kondisi
                            ekonomi dan politik serta pemenangan kandidat dalam pemilu. </p>
                        <!-- button -->
                        <a href="#aboutModal" data-toggle="modal"
                            class="dtr-btn btn-red dtr-mt-30 dtr-mr-40">Selengkapnya</a>
                        <!-- video button -->
                        {{-- <a class="dtr-video-popup dtr-text-btn dtr-btn-left-icon dtr-mt-30 color-red" data-autoplay="true"
                            data-vbtype="video" href="https://www.youtube.com/watch?v=wFZqCU_cHx8"><i
                                class="icon-play"></i>
                            Watch The Video</a> --}}
                    </div>
                    <!-- column 1 ends -->

                    <!-- column 2 starts -->
                    <div class="col-12 col-md-6 small-device-space"> <img src="{{ asset('assets/images/hero-img2.jpg') }}"
                            alt="image" class="dtr-rounded-img dtr-mb-minus50"> </div>
                    <!-- column 2 ends -->

                    <div class="modal fade" id="aboutModal" tabindex="-1" aria-labelledby="aboutModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="aboutModalLabel">Tentang Parameter Politik
                                        Indonesia (PPI)</h5>
                                    <button type="button" class="btn btn-sm" data-dismiss="modal" aria-label="Close">
                                        <i class="icon-window-close"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Parameter Politik Indonesia (PPI) adalah lembaga terpadu yangbergerak di
                                    bidang survei sosial-ekonomi dan political marketing. Lembaga ini ditujukan
                                    untuk memberi layanan survei seputar masalah kemasyarakatan, kondisi ekonomi
                                    dan politik serta pemenangan
                                    kandidat dalam pemilu. <br><br> Dengan mengedepankan tenaga handal serta
                                    berbasis metodologi yang bisa dipertanggungjawabkan, PPI dapat mengukur
                                    opini publik, memotret
                                    aspirasi masyarakat dan mendeteksi setiap arah perkembangan situasi sosial.
                                    Hasil survei dapat dijadikan referensi bagi pemangku kebijakan, perusahaan
                                    maupun kandidat dalam pemilu/pilkada. <br><br> Bagi pemangku
                                    kebijakan, hasil survei ppi dapat menjadi acuan dalam merumuskan dan
                                    mengevaluasi kebijakan Bagi perusahaan, data survel bran mengenal sikap
                                    menyajikan laporan dan kepuasaan pela gan terhadap pelanggan suatu
                                    produk Sedangkan bagi kandidat, data survei ibarat peta navigasi yang
                                    memberi gambaran soal peluang sekaligus tantangan di arena kontestasi
                                    pemilu/pilkada. <br><br> Kekuatan data survei PPI dilengkapi dengan
                                    rekomendasi strategis yang bisa digunakan oleh para pengguna jasa Strategi
                                    yang jitu, teruji dan terukur direkomendasikan secara eksklusif sesuai objek
                                    kajian.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--===== row 1 ends =====-->

            </div>
        </div>
    </section>
    <!-- hero section ends -->

    <!-- features section starts -->
    <section id="features" class="dtr-section dtr-pt-150 dtr-pb-100">
        <div class="container">

            <!-- heading starts -->
            <div class="dtr-styled-heading text-center">
                <h2>Keunggulan Kami</h2>
                <!-- <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor<br> incididunt soluta nobis assumenda labore quod maxime.</p> -->
            </div>
            <!-- heading ends -->

            <!--== row starts ==-->
            <div class="row dtr-pt-10">

                <!-- column 1 starts -->
                <div class="col-12 col-md-4 dtr-img-feature"> <span class="dtr-img-feature-img"><span
                            class="dtr-img-feature-circle bg-light-red"></span><i class="icon-search color-blue"></i></span>
                    <h4 class="dtr-img-feature-heading">Metodologi yang Tepat </h4>
                    <p>PPI dapat mengukur opini publik, memotret aspirasi masyarakat dan mendeteksi setiap arah
                        perkembangan situasi sosial. </p>
                </div>
                <!-- column 1 ends -->

                <!-- column 2 starts -->
                <div class="col-12 col-md-4 dtr-img-feature"> <span class="dtr-img-feature-img"><span
                            class="dtr-img-feature-circle bg-light-red"></span><i
                            class="icon-user-check1 color-blue"></i></span>
                    <h4 class="dtr-img-feature-heading">Tenaga Handal </h4>
                    <p>Diisi oleh para ahli yang kompeten dan handal dalam melakukan segala macam riset. </p>
                </div>
                <!-- column 2 ends -->

                <!-- column 3 starts -->
                <div class="col-12 col-md-4 dtr-img-feature"> <span class="dtr-img-feature-img"><span
                            class="dtr-img-feature-circle bg-light-red"></span><i
                            class="icon-thumbs-up2 color-blue"></i></span>
                    <h4 class="dtr-img-feature-heading">Berpengalaman </h4>
                    <p>Kami telah menyelesaikan banyak survei pada setiap acara pemilihan umum yang diadakan
                        oleh pemerintah seperti Pilpres, Pilek, Pilkada, Pemilihan DPD. </p>
                </div>
                <!-- column 3 ends -->

            </div>
            <!--== row ends ==-->

        </div>
    </section>
    <!-- features section ends -->

    <!-- sticky tabs starts -->
    <section id="services" class="dtr-sticky-tabs-wrapper">

        <!-- wrapping div for top shape image - easy to change color / no need to edit image - refer help doc -->
        <div class="dtr-top-shape-img" style="background-image: url({{ asset('assets/images/inner-nav-bg.svg') }});">

            <!-- tabs nav start -->
            <!-- <div class="dtr-sticky-tabs-nav">
                <div class="container">
                    <nav class="dtr-scrollspy-tabs">
                        <ul class="dtr-scrollspy nav justify-content-center dtr-sticky-tabs">
                            <li class="nav-item"> <a class="nav-link" href="#tab1"><i class="icon-file-text"></i>Survey</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#tab2"><i class="icon-user2"></i>Pemenangan</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#tab3"><i class="icon-layers"></i>Quick Count & Exit Pool</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#tab4"><i class="icon-layers"></i>Consultant Kebijakan</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#tab5"><i class="icon-layers"></i>Consultant Komunikasi</a> </li>
                        </ul>
                    </nav>
                </div>
            </div> -->
            <!-- tabs nav ends -->

            <!-- heading starts -->
            <div class="dtr-pt-50 text-center">
                <h2>Layanan Kami</h2>
            </div>
            <!-- heading ends -->

            <div data-target=".dtr-scrollspy-tabs">

                <!-- tab 1 starts -->
                <section id="tab1" class="dtr-sticky-tabs-section">
                    <div class="dtr-sticky-tabs-content">
                        <div class="container">
                            <div class="row">

                                <!-- column 1 starts -->
                                <div class="col-12 col-md-6"> <img class="dtr-rounded-img"
                                        src="{{ asset('assets/images/service-img1.png') }}" alt="image">
                                </div>
                                <!-- column 1 ends -->

                                <!-- column 2 starts -->
                                <div class="col-12 col-md-6 small-device-space">
                                    <!-- Heading -->
                                    <h2>Survei</h2>
                                    <div class="dtr-pl-50">
                                        <!-- Text -->
                                        <p>Survei dilakukan untuk mengetahui persepsi publik yang dimanfaatkan
                                            untuk mengukur popularitas, akseptabilitas dan elektabilitas
                                            kandidat atau partai dalam Pilpres, Pileg, Pilkada, pemilihan DPD,
                                            maupun
                                            kebijakan pemerintah. </p>
                                        <ul class="dtr-icon-list dtr-mt-30">
                                            <li><i class="icon-check-circle2 color-blue"></i>Survei Pilpres,
                                                Pileg, Pilkada, pemilihan DPD</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Survei kebijakan
                                                publik
                                            </li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Survei Sosial &
                                                Ekonomi
                                            </li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Survei Kualitatif
                                                berupa Focus Group Discussion (FGD)</li>
                                            <!-- <li><i class="icon-plus1 color-blue"></i>More tailored taskes</li> -->
                                        </ul>
                                        <!-- button -->
                                        {{-- <a href="#contact"
                                        class="dtr-btn btn-red dtr-scroll-link dtr-mt-30">Pesan
                                        Sekarang</a> --}}
                                    </div>
                                </div>
                                <!-- column 2 ends -->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- tab 1 ends -->

                <!-- tab 2 starts -->
                <section id="tab2" class="dtr-sticky-tabs-section">
                    <div class="dtr-sticky-tabs-content">
                        <div class="container">
                            <div class="row">

                                <!-- column 1 starts -->
                                <div class="col-12 col-md-6 small-device-space">
                                    <!-- Heading -->
                                    <h2>Pemenangan</h2>
                                    <div class="dtr-pl-50">
                                        <!-- Text -->
                                        <p>Pendampingan pemenangan pemilu merupakan layanan jasa PPI untuk para
                                            kandidat yang akan maju dalam pemilihan. Pendampingan dimulai dari
                                            penyusunan grand strategy, penyusunan program pemenangan, pelatihan
                                            tim relawan, budgeting, monitoring evaluasi, dan pencitraan
                                            kandidat. Semua tahapan pendampingan dilakukan secara akurat,
                                            terperinci, dan terukur sehingga efektif dan efesien. </p>
                                        <ul class="dtr-icon-list dtr-mt-30">
                                            <li><i class="icon-check-circle2 color-blue"></i>Pemenangan
                                                presiden
                                            </li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Pemenangan kepala
                                                daerah provinsi, kota dan kabupaten</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Pemenangan calon
                                                anggota legislatif dan anggota DPD</li>
                                            <!-- <li><i class="icon-plus1 color-blue"></i>More tailored taskes</li> -->
                                        </ul>
                                        <!-- button -->
                                        {{-- <a href="#contact"
                                        class="dtr-btn btn-red dtr-scroll-link dtr-mt-30">Pesan
                                        Sekarang</a> --}}
                                    </div>
                                </div>
                                <!-- column 1 ends -->

                                <!-- column 2 starts -->
                                <div class="col-12 col-md-6"> <img class="dtr-rounded-img"
                                        src="{{ asset('assets/images/service-img2.png') }}" alt="image">
                                </div>
                                <!-- column 2 ends -->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- tab 2 ends -->

                <!-- tab 3 starts -->
                <section id="tab3" class="dtr-sticky-tabs-section">
                    <div class="dtr-sticky-tabs-content">
                        <div class="container">
                            <div class="row">

                                <!-- column 1 starts -->
                                <div class="col-12 col-md-6"> <img class="dtr-rounded-img"
                                        src="{{ asset('assets/images/service-img3.png') }}" alt="image">
                                </div>
                                <!-- column 1 ends -->

                                <!-- column 2 starts -->
                                <div class="col-12 col-md-6 small-device-space">
                                    <!-- Heading -->
                                    <h3>Quick Count & Exit Pool</h3>
                                    <div class="dtr-pl-50">
                                        <!-- Text -->
                                        <p><b>Hitung cepat</b> (Quick Count) merupakan metode untuk melakukan
                                            perhitungan hasil pemilu dan pemilukada secara cepat dan akurat.
                                            Hitung cepat dilakukan di beberapa Tempat Penghitungan Suara (TPS)
                                            sebagai
                                            sampel pilihan. Meskipun penghitungan tidak dilakukan di semua TPS,
                                            namun hasilnya akan memiliki akurasi ketepatan dengan penghitungan
                                            versi penyelenggara pemilu. <br><br> <b>Exit Poll</b> merupakan
                                            survei yang dilakukan kepada pemilih sesaat setelah melakukan
                                            pemilihan di Tempat Pemungutan Suara (TPS) pada hari "H". Survei ini
                                            sekaligus untuk mendapatkan informasi mengenai perubahan sikap dan
                                            pemilihan
                                            politik pemilih pada saat survei dilakukan. </p>
                                        <!-- <ul class="dtr-icon-list dtr-mt-30">
                                            <li><i class="icon-check-circle2 color-blue"></i>Advertising for a trade magazine</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Designing marketing materials </li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Update website content</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Organise a client social event</li>
                                            <li><i class="icon-plus1 color-blue"></i>More tailored taskes</li>
                                        </ul> -->
                                        <!-- button -->
                                        {{-- <a href="#contact"
                                        class="dtr-btn btn-red dtr-scroll-link dtr-mt-30">Pesan
                                        Sekarang</a> --}}
                                    </div>
                                </div>
                                <!-- column 2 ends -->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- tab 3 ends -->

                <!-- tab 4 starts -->
                <section id="tab2" class="dtr-sticky-tabs-section">
                    <div class="dtr-sticky-tabs-content">
                        <div class="container">
                            <div class="row">

                                <!-- column 1 starts -->
                                <div class="col-12 col-md-6 small-device-space">
                                    <!-- Heading -->
                                    <h2>Konsultan Kebijakan</h2>
                                    <div class="dtr-pl-50">
                                        <!-- Text -->
                                        <p>PPI membantu suatu lembaga atau pejabat publik dalam menyusun,
                                            mengembangkan dan mengevaluasi kebijakan yang efektif. Jasa layanan
                                            mencangkup: </p>
                                        <ul class="dtr-icon-list dtr-mt-30">
                                            <li><i class="icon-check-circle2 color-blue"></i>Analisis perumusan
                                                kebijakan berbasis riset</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Desain dan
                                                penyusunan solusi yang detail dan komprehensif</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Pelatihan SDM dan
                                                technical assistant</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Evaluasi kebijakan
                                                publik
                                            </li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Perbaikan tata
                                                kelola kelembagaan</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Sosialisasi/kampanye
                                                kebijakan publik</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Leadership school
                                            </li>
                                            <!-- <li><i class="icon-plus1 color-blue"></i>More tailored taskes</li> -->
                                        </ul>
                                        <!-- button -->
                                        {{-- <a href="#contact"
                                        class="dtr-btn btn-red dtr-scroll-link dtr-mt-30">Pesan
                                        Sekarang</a> --}}
                                    </div>
                                </div>
                                <!-- column 1 ends -->

                                <!-- column 2 starts -->
                                <div class="col-12 col-md-6"> <img class="dtr-rounded-img"
                                        src="assets/images/service-img4.png" alt="image"> </div>
                                <!-- column 2 ends -->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- tab 4 ends -->

                <!-- tab 5 starts -->
                <section id="tab1" class="dtr-sticky-tabs-section">
                    <div class="dtr-sticky-tabs-content">
                        <div class="container">
                            <div class="row">

                                <!-- column 1 starts -->
                                <div class="col-12 col-md-6"> <img class="dtr-rounded-img"
                                        src="{{ asset('assets/images/service-img5.png') }}" alt="image">
                                </div>
                                <!-- column 1 ends -->

                                <!-- column 2 starts -->
                                <div class="col-12 col-md-6 small-device-space">
                                    <!-- Heading -->
                                    <h2>Konsultan Komunikasi</h2>
                                    <div class="dtr-pl-50">
                                        <!-- Text -->
                                        <p>PPI memberikan layanan komunikasi untuk membangun, menampilkan dan
                                            memelihara pencitraan personal, korporasi, maupun suatu institusi
                                            melalui: </p>
                                        <ul class="dtr-icon-list dtr-mt-30">
                                            <li><i class="icon-check-circle2 color-blue"></i>Kampanye iklan
                                                media
                                            </li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Media relations
                                            </li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Networking</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>PR</li>
                                            <li><i class="icon-check-circle2 color-blue"></i>Event management
                                            </li>
                                            <!-- <li><i class="icon-plus1 color-blue"></i>More tailored taskes</li> -->
                                        </ul>
                                        <!-- button -->
                                        {{-- <a href="#contact"
                                        class="dtr-btn btn-red dtr-scroll-link dtr-mt-30">Pesan
                                        Sekarang</a> --}}
                                    </div>
                                </div>
                                <!-- column 2 ends -->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- tab 5 ends -->

            </div>
        </div>
    </section>
    <!-- sticky tabs starts -->

    <!-- about section starts -->
    <section id="about" class="dtr-section dtr-pt-100 dtr-pb-150 dtr-section-with-bg"
        style="background-image: url({{ asset('assets/images/section-bg-img1.jpg') }});">
        <!-- blue overlay -->
        <div class="dtr-overlay dtr-overlay-blue"></div>
        <div class="container dtr-overlay-content">

            <!-- row starts -->
            <div class="row">

                <!-- column 1 starts -->
                <div class="col-12 col-md-6 col-lg-5">
                    <h2 class="color-white">Keutamaan PPI</h2>
                    <p class="color-white-muted">Selain tenaga yang handal dan pengalaman juga penggunaan
                        metodologi yang ketat dan hati-hati, rekomendasi strategi menjadi nilai keutamaan PPI.
                        Formulasi strategi sebagai tindak lanjut hasil survey bisa didapatkan dalam bentuk:</p>

                    <!--== accordion starts ==-->
                    <div class="dtr-mt-30 color-white">
                        <div class="dtr-accordion accordion" id="accord-index1">

                            <!-- accordion tab 1 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading1">
                                    <h4>
                                        <button class="dtr-btn accordion-btn-link collapsed" type="button"
                                            data-toggle="collapse" data-target="#accord-index1-collapse1"
                                            aria-expanded="false" aria-controls="accord-index1-collapse1">Perumusan
                                            Kebijakan</button>
                                    </h4>
                                </div>
                                <div id="accord-index1-collapse1" class="collapse"
                                    aria-labelledby="accord-index1-heading1" data-parent="#accord-index1">
                                    <div class="card-body">Perumusan kebijakan lanjutan yang lebih efektif
                                        dan efisien bagi pemerintah.</div>
                                </div>
                            </div>
                            <!-- accordion tab 1 ends -->

                            <!-- accordion tab 2 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading2">
                                    <h4>
                                        <button class="dtr-btn accordion-btn-link collapsed" type="button"
                                            data-toggle="collapse" data-target="#accord-index1-collapse2"
                                            aria-expanded="false" aria-controls="accord-index1-collapse2">Pemasaran
                                            Produk</button>
                                    </h4>
                                </div>
                                <div id="accord-index1-collapse2" class="collapse"
                                    aria-labelledby="accord-index1-heading2" data-parent="#accord-index1">
                                    <div class="card-body">Pemasaran produk dan pemetaan segemntasi secara
                                        tepat bagi perusahaan.</div>
                                </div>
                            </div>
                            <!-- accordion tab 2 ends -->

                            <!-- accordion tab 3 starts -->
                            <div class="card">
                                <div class="card-header" id="accord-index1-heading3">
                                    <h4>
                                        <button class="dtr-btn accordion-btn-link collapsed" type="button"
                                            data-toggle="collapse" data-target="#accord-index1-collapse3"
                                            aria-expanded="false" aria-controls="accord-index1-collapse3">Pengenalan
                                            Kandidat</button>
                                    </h4>
                                </div>
                                <div id="accord-index1-collapse3" class="collapse"
                                    aria-labelledby="accord-index1-heading3" data-parent="#accord-index1">
                                    <div class="card-body">Pengenalan kandidat, managemen isu, organisasi
                                        jaringan, kampanye dan sejenisnya yang efektif bagi kandidat.</div>
                                </div>
                            </div>
                            <!-- accordion tab 3 ends -->

                        </div>
                    </div>
                    <!--== accordion ends ==-->

                    <p class="color-white-muted dtr-mt-40">"Selain tenaga yang handal dan pengalaman juga
                        penggunaan metodologi yang ketat dan hati-hati, rekomendasi strategi menjadi nilai
                        keutamaan PPI."</p>

                    <!-- author box starts -->
                    <div class="dtr-author-box dtr-mt-20"> <img src="{{ asset('assets/images/adi-prayitno.png') }}"
                            width="60" height="60" alt="image" class="dtr-author-img border-red">
                        <div class="dtr-author-content">
                            <h6 class="color-white">Adi Prayitno</h6>
                            <p class="color-white-muted">Direktur Eksekutif</p>
                        </div>
                        <!-- <img src="assets/images/signature.png" alt="image" class="ml-auto">  -->
                    </div>
                    <!-- author box ends -->

                </div>
                <!-- column 1 ends -->

                <!-- column 2 starts -->
                <div class="col-12 col-md-6 col-lg-6 offset-lg-1 small-device-space dtr-rounded-img"> <img
                        src="assets/images/about-img.png" alt="image"> </div>
                <!-- column 2 ends -->

            </div>
            <!-- row ends -->

        </div>
    </section>
    <!-- about section ends -->

    <!-- cta section starts -->
    <section class="dtr-section dtr-mb-90 dtr-mt-90">
        <div class="container">

            <!-- cta box starts -->
            <div class="dtr-cta-box bg-red color-white">
                <div class="dtr-cta-box-left" style="background-image: url({{ asset('assets/images/cta_bg.jpg') }});">
                </div>
                <div class="dtr-cta-box-right">
                    <div class="dtr-cta-box-icon border-white-muted color-red"></div>
                    <p class="dtr-mb-20">Hubungi kami untuk informasi lebih lanjut di</p>
                    <p class="dtr-cta-title">021 75682929</p>
                </div>
            </div>
            <!-- cta box ends -->

        </div>
    </section>
    <!-- cta section ends -->

    <!-- contact section starts -->
    <!-- top background curve image - easy to change color / no need to edit image - refer help doc -->
    <section id="contact" class="dtr-section dtr-section-with-bg bg-blue color-white"
        style="background-image: url({{ asset('assets/images/white-shape-bg.png') }});">

        <!-- wrapping div for top shape image - easy to change color / no need to edit image - refer help doc -->
        <div class="dtr-py-100 dtr-top-shape-img"
            style="background-image: url({{ asset('assets/images/contact-section-top.svg') }});">
            <div class="container">

                <!--== row starts ==-->
                <div class="row">

                    <!-- column 1 starts -->
                    <div class="col-12 col-md-8">

                        <!-- heading starts -->
                        <div class="dtr-styled-heading">
                            <h2>Dimana kami dapat menghubungi anda?</h2>
                            <p class="color-white-muted">Tinggalkan pesan anda di bawah ini, dan kami akan
                                membalasnya segera.</p>
                        </div>
                        <!-- heading ends -->

                        <!-- form starts -->
                        <div class="dtr-form dtr-form-styled dtr-form-dark-bg">
                            <form id="contactform" method="post" action="php/contact-form.php">
                                <fieldset>

                                    <!-- form two columns start -->
                                    <div class="dtr-form-row dtr-form-row-2col clearfix">
                                        <div class="dtr-form-column">
                                            <p class="dtr-form-field"> <span class="dtr-form-subtext">Nama
                                                    Anda</span>
                                                <input name="name" type="text" placeholder="Indra Oktaviano">
                                            </p>
                                        </div>
                                        <div class="dtr-form-column">
                                            <p class="dtr-form-field"> <span class="dtr-form-subtext">Email
                                                    Anda</span>
                                                <input name="email" class="required email" type="text"
                                                    placeholder="indra@example.com">
                                            </p>
                                        </div>
                                    </div>
                                    <!-- form two columns ends -->

                                    <p class="dtr-form-field"> <span class="dtr-form-subtext">Subjek</span>
                                        <input name="subject" type="text" placeholder="Subjek">
                                    </p>
                                    <p class="antispam">Leave this empty: <br />
                                        <input name="url" />
                                    </p>
                                    <p class="dtr-form-field"> <span class="dtr-form-subtext">Pesan
                                            Anda</span>
                                        <textarea rows="6" name="message" id="message" class="required"
                                            placeholder="Isi pesan anda"></textarea>
                                    </p>
                                    <p class="text-center">
                                        <button class="dtr-btn btn-red w-100" type="submit"> Kirim Pesan <i
                                                class="icon-arrow-right-circle dtr-ml-10"></i> </button>
                                    </p>
                                    <div id="result"></div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- form ends -->

                    </div>
                    <!-- column 1 ends -->

                    <!-- column 2 starts -->
                    <div class="col-12 col-md-4">
                        <div class="dtr-pl-50"> <img src="{{ asset('assets/images/illustration.png') }}"
                                alt="image" class="small-device-space">
                            <h5 class="dtr-mt-50">Cara lain untuk menghubungi</h5>

                            <!-- contact box starts -->
                            <div class="dtr-contact-box"> <i class="icon-whatsapp"></i>
                                <div class="dtr-contact-box-content color-white"> <span
                                        class="dtr-contact-box-title color-white-muted">Whatsapp</span> <a
                                        href="https://api.whatsapp.com/send?phone=6281318940139&text=Hai%20Parameter%20Politik%20Indonesia...."
                                        target="_blank">0813 1894 0139</a> </div>
                            </div>
                            <!-- contact box ends -->

                        </div>
                    </div>
                    <!-- column 2 ends -->

                </div>
                <!--== row ends ==-->

            </div>
        </div>
    </section>
    <!-- contact section ends -->
@endsection
