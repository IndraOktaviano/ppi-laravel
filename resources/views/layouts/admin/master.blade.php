<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') Dashboard PPI</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/icons/favicon.png') }}">

    <!-- page css -->
    <style>
        .side-nav-menu>.active>a {
            color: aqua;
            background-color: rgba(63, 135, 245, 0.15);
        }

        .nav-item>a:hover {
            color: aqua;
        }

    </style>
    @yield('css')

    <!-- Core css -->
    <link href="{{ asset('assets/admin/css/app.min.css') }}" rel="stylesheet">

</head>

<body>
    <div class="app">
        <div class="layout">
            <!-- Header START -->
            <div class="header">
                <div class="logo logo-dark">
                    <a href="{{ route('admin.index') }}">
                        <img src="{{ asset('assets/admin/images/logo/ppi-logo.png') }}" class="img-fluid"
                            alt="Logo">
                        <img class="logo-fold img-fluid" src="{{ asset('assets/admin/images/logo/logo-fold.png') }}"
                            alt="Logo">
                    </a>
                </div>
                <div class="logo logo-white">
                    <a href="{{ route('admin.index') }}">
                        <img src="{{ asset('assets/admin/images/logo/logo-white.png') }}" alt="Logo">
                        <img class="logo-fold" src="{{ asset('assets/admin/images/logo/logo-fold-white.png') }}"
                            alt="Logo">
                    </a>
                </div>
                <div class="nav-wrap">
                    <ul class="nav-left">
                        <li class="desktop-toggle">
                            <a href="javascript:void(0);">
                                <i class="anticon"></i>
                            </a>
                        </li>
                        <li class="mobile-toggle">
                            <a href="javascript:void(0);">
                                <i class="anticon"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="dropdown dropdown-animated scale-left">
                            <div class="pointer" data-toggle="dropdown">
                                <div class="avatar avatar-image  m-h-10 m-r-15" style="color: blue">
                                    <i class="fas primary fa-user-shield"></i>
                                </div>
                            </div>
                            <div class="p-b-15 p-t-20 dropdown-menu pop-profile">
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                    class="dropdown-item d-block p-h-15 p-v-10">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div>
                                            <i class="anticon opacity-04 font-size-16 anticon-logout"></i>
                                            <span class="m-l-10">Logout</span>
                                        </div>
                                        <i class="anticon font-size-10 anticon-right"></i>
                                    </div>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Header END -->

            <!-- Side Nav START -->
            <div class="side-nav">
                <div class="side-nav-inner">
                    <ul class="side-nav-menu scrollable">
                        <li class="nav-item dropdown {{ request()->is('admin') ? 'active' : '' }}">
                            <a href="{{ url('admin') }}">
                                <span class="icon-holder">
                                    <i class="anticon anticon-dashboard"></i>
                                </span>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown {{ request()->is('admin/blog/*') ? 'open' : '' }}">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="far fa-newspaper"></i>
                                </span>
                                <span class="title">Article</span>
                                <span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li
                                    class="{{ request()->is('admin/blog') || request()->is('admin/blog/*/edit') ? 'active' : '' }}">
                                    <a href="{{ route('admin.blog.index') }}">Index</a>
                                </li>
                                <li class="{{ request()->is('admin/blog/create') ? 'active' : '' }}">
                                    <a href="{{ route('admin.blog.create') }}">Create</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown {{ request()->is('admin/gallery/*') ? 'open' : '' }}">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="fas fa-images"></i>
                                </span>
                                <span class="title">Gallery</span>
                                <span class="arrow">
                                    <i class="arrow-icon"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li
                                    class="{{ request()->is('admin/gallery') || request()->is('admin/gallery/*/edit') ? 'active' : '' }}">
                                    <a href="{{ route('admin.gallery.index') }}">Index</a>
                                </li>
                                <li class="{{ request()->is('admin/gallery/create') ? 'active' : '' }}">
                                    <a href="{{ route('admin.gallery.create') }}">Create</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Side Nav END -->

            <!-- Page Container START -->
            <div class="page-container">

                <!-- Content Wrapper START -->
                <div class="main-content">
                    <div class="page-header">
                        <h2 class="header-title">@yield('title-bar')</h2>
                        <div class="header-sub-title">
                            <nav class="breadcrumb breadcrumb-dash">
                                @yield('breadcrumb')
                                {{-- <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a> --}}
                                {{-- <a class="breadcrumb-item" href="#">Breadcrumb 1</a>
                                <span class="breadcrumb-item active">Breadcrumb 2</span> --}}
                            </nav>
                        </div>
                    </div>
                    <!-- Content goes Here -->
                    @if (session('message'))
                        <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true">
                            <div class="toast-header">
                                <i class="anticon anticon-info-circle text-primary m-r-5"></i>
                                <strong class="mr-auto">Modernesia</strong>
                                <small class="text-muted">just now</small>
                                <button type="button" class="ml-2 close" data-dismiss="toast" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="toast-body">
                                {{ session('message') }}
                            </div>
                        </div>
                    @endif
                    @yield('content')
                </div>
                <!-- Content Wrapper END -->

                <!-- Footer START -->
                <footer class="footer">
                    <div class="footer-content">
                        <p class="m-b-0">Copyright © 2019 Theme_Nate. All rights reserved.</p>
                        <span>
                            <a href="" class="text-gray m-r-15">Term &amp; Conditions</a>
                            <a href="" class="text-gray">Privacy &amp; Policy</a>
                        </span>
                    </div>
                </footer>
                <!-- Footer END -->

            </div>
            <!-- Page Container END -->

            <!-- Search Start-->
            <div class="modal modal-left fade search" id="search-drawer">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header justify-content-between align-items-center">
                            <h5 class="modal-title">Search</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <i class="anticon anticon-close"></i>
                            </button>
                        </div>
                        <div class="modal-body scrollable">
                            <div class="input-affix">
                                <i class="prefix-icon anticon anticon-search"></i>
                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                            <!-- Content goes Here -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Search End-->

            <!-- Quick View START -->
            <div class="modal modal-right fade quick-view" id="quick-view">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header justify-content-between align-items-center">
                            <h5 class="modal-title">Quick View</h5>
                        </div>
                        <div class="modal-body scrollable">
                            <!-- Content goes Here -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Quick View END -->
        </div>
    </div>


    <!-- Core Vendors JS -->
    <script src="{{ asset('assets/admin/js/vendors.min.js') }}"></script>

    <!-- page js -->
    @yield('js')

    <!-- Core JS -->
    <script src="{{ asset('assets/admin/js/app.min.js') }}"></script>

</body>

</html>
