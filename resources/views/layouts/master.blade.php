<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title>@yield('title') Parameter Politik Indonesia</title>
    <meta name="author" content="Parameter Politik Indonesia">
    <meta name="description" content="Penyedia Layanan Riset dan Konsultan">
    <meta name="keywords" content="research, campaign, political, business consultant">

    <!-- FAVICON FILES -->
    <link href="{{ asset('assets/images/icons/ppi-logo-144') }}" rel="ppi-logo" sizes="144x144">
    <link href="{{ asset('assets/images/icons/ppi-logo-120.png') }}" rel="ppi-logo" sizes="120x120">
    <link href="{{ asset('assets/images/icons/ppi-logo-76.png') }}" rel="ppi-logo" sizes="76x76">
    <link href="{{ asset('assets/images/icons/favicon.png') }}" rel="shortcut icon">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/iconfonts.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/color.css') }}">
    @yield('css')
</head>

<body>
    <div id="dtr-wrapper" class="clearfix">

        <!-- preloader starts -->
        <div class="dtr-preloader">
            <div class="dtr-preloader-inner">
                <div class="dtr-loader">Loading...</div>
            </div>
        </div>
        <!-- preloader ends -->

        <!-- Small Devices Header -->
        <div class="dtr-responsive-header fixed-top">
            <div class="container">

                <!-- small devices logo -->
                <a href="{{ url('/') }}"><img src="{{ asset('assets/images/ppi-dark.png') }}" alt="logo"></a>
                <!-- small devices logo ends -->

                <!-- menu button -->
                <button id="dtr-menu-button" class="dtr-hamburger" type="button"><span
                        class="dtr-hamburger-lines-wrapper"><span class="dtr-hamburger-lines"></span></span></button>
            </div>
            <div class="dtr-responsive-header-menu"></div>
        </div>
        <!-- Small Devices Header ends -->

        <!-- Header -->
        <header id="dtr-header-global" class="fixed-top trans-header">
            <div class="container">
                <div class="d-flex align-items-center justify-content-between">

                    <!-- header left starts -->
                    <div class="dtr-header-left">

                        <!-- logo -->
                        <a class="logo-default" href="{{ url('/') }}"><img
                                src="{{ asset('assets/images/ppi-light.png') }}" width="200px" alt="logo"></a>

                        <!-- logo on scroll -->
                        <a class="logo-alt" href="{{ url('/') }}"><img
                                src="{{ asset('assets/images/ppi-logo.png') }}" width="200px" alt="logo"></a>
                        <!-- logo on scroll ends -->

                    </div>
                    <!-- header left ends -->

                    <!-- menu starts-->
                    <div class="dtr-header-right ml-auto">
                        <div class="main-navigation dtr-menu-light">
                            @if (Request::is('/'))
                                <ul class="sf-menu dtr-scrollspy dtr-nav light-nav-on-load dark-nav-on-scroll">
                                    <li> <a class="nav-link" href="#home">Home</a> </li>
                                    <li> <a class="nav-link" href="#services">Services</a> </li>
                                    <li> <a class="nav-link" href="#about">About</a> </li>
                                    <li> <a class="nav-link" href="{{ route('blog.index') }}">Publikasi</a>
                                    </li>
                                    <li> <a class="nav-link" href="{{ route('gallery.index') }}">Gallery</a>
                                    </li>
                                    <li> <a class="nav-link" href="#contact">Contact</a> </li>
                                </ul>
                            @endif
                            @if (Request::is('blog') || Request::is('blog/*'))
                                <ul class="sf-menu dtr-scrollspy dtr-nav light-nav-on-load dark-nav-on-scroll">
                                    <li> <a class="nav-link" href="{{ route('index') }}#home">Home</a> </li>
                                    <li> <a class="nav-link" href="{{ route('index') }}#services">Services</a>
                                    </li>
                                    <li> <a class="nav-link" href="{{ route('index') }}#about">About</a> </li>
                                    <li> <a class="nav-link active" href="{{ route('blog.index') }}">Publikasi</a>
                                    </li>
                                    <li> <a class="nav-link" href="{{ route('gallery.index') }}">Gallery</a>
                                    </li>
                                    <li> <a class="nav-link" href="{{ route('index') }}#contact">Contact</a>
                                    </li>
                                </ul>
                            @endif
                            @if (Request::is('gallery') || Request::is('gallery/*'))
                                <ul class="sf-menu dtr-scrollspy dtr-nav light-nav-on-load dark-nav-on-scroll">
                                    <li> <a class="nav-link" href="{{ route('index') }}#home">Home</a> </li>
                                    <li> <a class="nav-link" href="{{ route('index') }}#services">Services</a>
                                    </li>
                                    <li> <a class="nav-link" href="{{ route('index') }}#about">About</a> </li>
                                    <li> <a class="nav-link" href="{{ route('blog.index') }}">Publikasi</a>
                                    </li>
                                    <li> <a class="nav-link active" href="{{ route('gallery.index') }}">Gallery</a>
                                    </li>
                                    <li> <a class="nav-link" href="{{ route('index') }}#contact">Contact</a>
                                    </li>
                                </ul>
                            @endif
                        </div>
                    </div>
                    <!-- menu ends -->

                    <!-- header button starts -->
                    {{-- <a href="tel:+6281318940139" class="dtr-btn btn-red dtr-btn-right-icon dtr-ml-30">0813 1894 0139 <i
                            class="icon-phone-call"></i></a> --}}
                    <!-- header button ends -->

                </div>
            </div>
        </header>
        <!-- header ends -->

        <!-- == main content area starts == -->
        <div id="dtr-main-content">

            @yield('content')

            <!-- footer section starts -->
            <footer id="dtr-footer">

                <!--== footer main starts ==-->
                <div class="dtr-footer-main">
                    <div class="container">

                        <!--== row starts ==-->
                        <div class="row">

                            <!-- column 1 starts -->
                            <div class="col-12 col-md-8 small-device-space"> <img class="img-footer"
                                    src="{{ asset('assets/images/ppi-light.png') }}" alt="logo">
                                <p class="dtr-mt-30">Parameter Politik Indonesia (PPI) adalah lembaga terpadu yang
                                    bergerak di bidang survei sosial-ekonomi dan political marketing. Lembaga ini
                                    ditujukan untuk memberi layanan survei seputar masalah kemasyarakatan, kondisi
                                    ekonomi dan politik serta pemenangan kandidat dalam pemilu.</p>

                                <!-- social starts -->
                                <div class="dtr-social-large dtr-mt-30">
                                    <ul class="dtr-social dtr-social-list text-left">
                                        <li>
                                            <a href="#" class="dtr-facebook" target="_blank" title="facebook"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="dtr-instagram" target="_blank" title="instagram"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="dtr-twitter" target="_blank" title="twitter"></a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- social ends -->

                            </div>
                            <!-- column 1 ends -->

                            <!-- column 2 starts -->
                            <div class="col-12 col-md-4 small-device-space">
                                <h4>Info Kontak</h4>
                                <div class="spacer-30"></div>
                                <ul class="dtr-contact-widget">
                                    <li><i class="icon-phone-call"></i><a href="tel:+622175682929">021 75682929</a>
                                    </li>
                                    <li><i class="icon-envelope1"></i><a href="mailto:#">support@example.com</a></li>
                                    <li><i class="icon-map-pin1"></i>Graha Mustika Ratu, Suite 707<br> Jl. Gatot
                                        Subroto Kav. 74-75, Jakarta 12870</li>
                                </ul>
                            </div>
                            <!-- column 2 ends -->

                        </div>
                        <!--== row ends ==-->

                    </div>
                </div>
                <!--== footer main ends ==-->

                <!--== copyright starts ==-->
                <div class="dtr-copyright">
                    <div class="container">

                        <!--== row starts ==-->
                        <div class="row">

                            <!-- column 1 starts -->
                            <div class="col-12 text-center text-size-sm">
                                <p> Copyright © Parameter Politik Indonesia 2021. Template by <a
                                        href="https://themeforest.net/user/tansh">Tansh</a>.</p>
                            </div>
                            <!-- column 1 ends -->

                        </div>
                        <!--== row ends ==-->

                    </div>
                </div>
                <!--== copyright ends ==-->

            </footer>
            <!-- footer section ends -->

        </div>
        <!-- == main content area ends == -->

    </div>
    <!-- #dtr-wrapper ends -->

    <!-- JS FILES -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>
