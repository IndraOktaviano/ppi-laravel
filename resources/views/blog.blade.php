@extends('layouts.master')

@section('title', 'Publikasi - ')

@section('content')
    <!-- hero section starts -->
    <section id="publikasi" class="dtr-section dtr-section-with-bg dtr-hero-section-top-padding bg-blue"
        style="background-image: url({{ asset('assets/images/white-shape-bg.png') }});">

        <!-- wrapping div for background bottom curve stripes image - easy to change color - no need to edit image - refer help doc -->
        <div class="dtr-bottom-shape-img" style="background-image: url({{ asset('assets/images/hero-bottom.svg') }});">
            <div class="container">

                <!--===== row 1 starts =====-->
                <div class="row">

                    <!-- column 1 starts -->
                    <div class="col-12 text-center">

                        <!-- intro text -->
                        <h1 class="color-white">Publikasi</h1>
                        <br>
                        <br>

                    </div>
                    <!-- column 1 ends -->

                </div>
                <!--===== row 1 ends =====-->

            </div>
        </div>
    </section>
    <!-- hero section ends -->

    <!-- blog section starts -->
    <section id="publikasi" class="dtr-section dtr-py-100">
        <div class="container">

            <!-- heading starts -->
            <div class="dtr-styled-heading text-center">
                <h2>Recent Articles</h2>
            </div>
            <!-- heading ends -->

            <!--== row starts ==-->
            <div class="row dtr-mt-0">

                <!-- column 1 starts -->
                @foreach ($data as $item)
                <div class="col-12 col-md-4">

                    <!-- blog item 1 starts -->
                    <div class="dtr-blog-item">
                        <!-- image -->
                        <a href="{{ route('blog.show', $item->slug) }}">
                            <div class="dtr-post-img"> <img src="{{ asset('media/images/'.$item->image) }}"
                                    alt="image">
                                <span class="dtr-blog-date">{{date('d M'), strtotime($item->created_at)}} <br>
                                    {{date('Y'), strtotime($item->created_at)}}</span>
                            </div>
                            <!-- content -->
                            <h5>{{$item->title}}</h5>
                        </a>
                        {{-- <p class="text-truncate">{!!$item->desc!!}</p> --}}
                    </div>
                    <!-- blog item 1 ends -->

                </div>
                @endforeach
                <!-- column 1 ends -->

            </div>
            <!--== row starts ==-->

        </div>
    </section>
    <!-- blog section ends -->
@endsection
