@extends('layouts.master')

@section('title', 'Gallery - ')

@section('content')
    <!-- hero section starts -->
    <section id="publikasi" class="dtr-section dtr-section-with-bg dtr-hero-section-top-padding bg-blue"
        style="background-image: url({{ asset('assets/images/white-shape-bg.png') }});">

        <!-- wrapping div for background bottom curve stripes image - easy to change color - no need to edit image - refer help doc -->
        <div class="dtr-bottom-shape-img" style="background-image: url({{ asset('assets/images/hero-bottom.svg') }});">
            <div class="container">

                <!--===== row 1 starts =====-->
                <div class="row">

                    <!-- column 1 starts -->
                    <div class="col-12 text-center">

                        <!-- intro text -->
                        <h1 class="color-white">Gallery</h1>
                        <br>
                        <br>

                    </div>
                    <!-- column 1 ends -->

                </div>
                <!--===== row 1 ends =====-->

            </div>
        </div>
    </section>
    <!-- hero section ends -->

    <!-- blog section starts -->
    <section id="publikasi" class="dtr-section dtr-py-100">
        <div class="container">

            <!--== row starts ==-->
            <div class="row dtr-mt-0">

                <!-- column 1 starts -->
                @foreach ($data as $item)
                    <div class="col-12 col-md-3">

                        <!-- blog item 1 starts -->
                        <div class="dtr-blog-item">
                            <!-- image -->
                            {{-- <a href=""> --}}
                            <button type="button" class="btn" data-toggle="modal"
                                data-target="#galleryModal{{ $item->id }}">
                                <div class="polaroid">
                                    <img src="{{ asset('media/images/' . $item->image) }}" alt="{{ $item->title }}"
                                        class="img-fluid">
                                    <div class="container">
                                        <p>{{ $item->title }}</p>
                                    </div>
                                </div>
                            </button>
                            {{-- </a> --}}
                        </div>
                        <!-- blog item 1 ends -->

                    </div>
                @endforeach
                <!-- column 1 ends -->

            </div>
            <!--== row starts ==-->

        </div>
    </section>
    <!-- blog section ends -->

    <!-- Modal -->
    @foreach ($data as $item)
        <div class="modal fade" id="galleryModal{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="galleryModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="galleryModalLabel">{{ $item->title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="{{ asset('media/images/' . $item->image) }}" alt="{{ $item->title }}"
                            class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('css')
    <style>
        div.polaroid {
            width: 250px;
            height: 100%;
            background-color: white;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            margin-bottom: 25px;
        }

        div.container {
            text-align: center;
            padding: 10px 20px;
        }

        button:focus {
            outline: 0;
        }

    </style>
@endsection
