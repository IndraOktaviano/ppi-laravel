@extends('layouts.master')

@section('title')
    {{$data->title}} -
@endsection

@section('content')
    <!-- hero section starts -->
    <section id="blog" class="dtr-section dtr-section-with-bg dtr-hero-section-top-padding bg-blue"
        style="background-image: url({{ asset('assets/images/white-shape-bg.png') }});">

        <!-- wrapping div for background bottom curve stripes image - easy to change color - no need to edit image - refer help doc -->
        <div class="dtr-bottom-shape-img" style="background-image: url({{ asset('assets/images/hero-bottom.svg') }});">
            <div class="container">

                <!--===== row 1 starts =====-->
                <div class="row">

                    <!-- column 1 starts -->
                    <div class="col-12 text-center">

                        <!-- intro text -->
                        <h1 class="color-white">{{$data->title}}</h1>
                        <br>
                        <br>
                    </div>
                    <!-- column 1 ends -->

                </div>
                <!--===== row 1 ends =====-->

            </div>
        </div>
    </section>
    <!-- hero section ends -->

    <!-- blog section starts -->
    <section id="blog" class="dtr-section dtr-py-100">
        <div class="container">

            <!--== row starts ==-->
            <div class="row dtr-mt-0">

                <!-- column 1 starts -->
                <div class="col-12 mb-5">
                    <img src="{{ asset('media/images/'.$data->image) }}" alt="image" style="width: 100%">
                </div>
                <!-- column 1 ends -->

                <!-- column 2 starts -->
                <div class="col-12">
                    {{date('D, d F Y'), strtotime($data->created_at)}}
                    <br>
                    <br>
                    {!!$data->desc!!}
                    <br>
                    <br>
                    <div class="btn-group" role="group" aria-label="Lampiran">
                        <a href="{{ asset('media/archives/'.$data->archive) }}" class="btn btn-primary" download="{{$data->title}}">
                            <i class="icon-download"></i>
                        </a>
                        <a href="{{ asset('media/archives/'.$data->archive) }}" target="_blank" class="btn btn-blue">Lihat Lampiran</a>
                    </div>

                </div>
                <!-- column 2 ends -->

            </div>
            <!--== row starts ==-->

        </div>
    </section>
    <!-- blog section ends -->
@endsection
