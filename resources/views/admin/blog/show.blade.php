@extends('layouts.admin.master')

@section('title')
    {{ $data->title }} -
@endsection

@section('title-bar', 'Article')

@section('breadcrumb')
    <a href="#" class="breadcrumb-item"><i class="far fa-newspaper m-r-5"></i>Article</a>
    <a href="{{ route('admin.blog.index') }}" class="breadcrumb-item">Index</a>
    <span class="breadcrumb-item active">{{ $data->title }}</span>
@endsection

@section('content')
    {{-- <form method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="title" name="title" value="{{ $data->title }}"
                    placeholder="title" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="desc" class="col-sm-2 col-form-label">Desc</label>
            <div class="col-sm-10">
                <textarea id="mytextarea" name="desc">{{ $data->desc }}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="image" class="col-sm-2 col-form-label">Image</label>
            <div class="col-sm-9 ml-3 custom-file">
                <input type="file" name="image" value="{{ $data->image }}" class="custom-file-input" id="customFile"
                    required>
                <label class="custom-file-label" for="customFile">{{ $data->image }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="archive" class="col-sm-2 col-form-label">Archive</label>
            <div class="col-sm-9 ml-3 custom-file">
                <input type="file" name="archive" value="{{ $data->archive }}" class="custom-file-input" id="customFile"
                    required>
                <label class="custom-file-label" for="customFile">
                    {{ $data->archive != null ? $data->archive : '-' }}
                </label>
            </div>
        </div>
    </form> --}}
    <div class="card">
        <div class="card-body">
            <div class="container">
                <h2 class="font-weight-normal m-b-10">{{$data->title}}</h2>
                <div class="d-flex m-b-10">
                    <p class="m-b-0 text-muted font-size-13">{{date('M d, Y'), strtotime($data->created_at)}}</p>
                </div>
                <img alt="" class="img-fluid w-100" src="{{asset('media/images/'.$data->image)}}">
                <div class="m-t-10">
                    {!! $data->desc !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
    <script src="{{ asset('assets/admin/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
@endsection
