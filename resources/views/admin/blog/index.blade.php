@extends('layouts.admin.master')

@section('title', 'Article - ')

@section('title-bar', 'Article')

@section('breadcrumb')
    <a href="#" class="breadcrumb-item"><i class="far fa-newspaper m-r-5"></i>Article</a>
    <span class="breadcrumb-item active">Index</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <h4>Article</h4>
                <a href="{{ route('admin.blog.create') }}" class="btn btn-primary btn-rounded" id="trigger-loading-1">
                    <i class="fas fa-plus-circle"></i> New Article
                    <i class="anticon anticon-loading"></i>
                </a>
            </div>
            <div class="m-t-25">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Archive</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ date('d M Y'), strtotime($item->created_at) }}</td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    <img src="{{ asset('media/images/' . $item->image) }}" alt="" style="width: 100px">
                                </td>
                                <td>
                                    @if ($item->archive == true)
                                        <a href="{{ asset('media/archives/' . $item->archive) }}" target="_blank"
                                            class="btn btn-sm btn-info">
                                            Lihat Lampiran
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <a href="{{ route('admin.blog.show', $item->id) }}" title="Show"
                                            class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                        <a href="{{ route('admin.blog.edit', $item->id) }}"
                                            class="btn btn-sm btn-secondary" title="Edit"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <form onsubmit="return confirm('Are you sure to delete {{ $item->title }}?')"
                                            action="{{ route('admin.blog.destroy', $item->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger" title="Delete"><i
                                                    class="fas fa-trash"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Archive</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/admin/vendors/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('assets/admin/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendors/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $('#data-table').DataTable();
    </script>
    <script>
        $('#trigger-loading-1').on('click', function(e) {
            $(this).addClass("is-loading");
            setTimeout(function() {
                $("#trigger-loading-1").removeClass("is-loading");
            }, 4000);
            // e.preventDefault();
        });
    </script>
@endsection
