@extends('layouts.admin.master')

@section('title', 'New Image - ')

@section('title-bar', 'New Image')

@section('breadcrumb')
    <a href="#" class="breadcrumb-item"><i class="far fa-newspaper m-r-5"></i>Image</a>
    <a href="{{ route('admin.gallery.index') }}" class="breadcrumb-item">Index</a>
    <span class="breadcrumb-item active">New Image</span>
@endsection

@section('content')
    <form action="{{ route('admin.gallery.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="title" name="title" placeholder="title" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="image" class="col-sm-2 col-form-label">Image</label>
            <div class="col-sm-9 ml-3 custom-file">
                <input type="file" name="image" class="custom-file-input" id="customFile" required>
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
        <div class="form-group row d-flex justify-content-end">
            <div class="col-sm-10">
                <a href="{{ url()->previous() }}" class="btn btn-secondary btn-tone">Back</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script>
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
@endsection
