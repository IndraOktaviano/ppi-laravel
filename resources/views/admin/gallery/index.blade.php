@extends('layouts.admin.master')

@section('title', 'Gallery -')

@section('title-bar', 'Gallery')

@section('breadcrumb')
    <a href="#" class="breadcrumb-item"><i class="fas fa-images m-r-5"></i>Gallery</a>
    <span class="breadcrumb-item active">Index</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <h4>Gallery</h4>
                <a href="{{ route('admin.gallery.create') }}" class="btn btn-primary btn-rounded" id="trigger-loading-1">
                    <i class="fas fa-plus-circle"></i> New Gallery
                    <i class="anticon anticon-loading"></i>
                </a>
            </div>
            <div class="m-t-25">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ date('d M Y'), strtotime($item->created_at) }}</td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    <img src="{{ asset('media/images/' . $item->image) }}" alt="" style="width: 100px">
                                </td>
                                <td>
                                    <div class="d-flex justify-content-start align-items-center">
                                        {{-- <a href="{{ route('admin.gallery.show', $item->id) }}" title="Show"
                                            class="btn btn-sm ml-3 btn-info"><i class="fas fa-eye"></i></a> --}}
                                        <button type="button" class="btn btn-sm btn-info" data-toggle="modal"
                                            data-target="#galleryModal{{$item->id}}">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                        <a href="{{ route('admin.gallery.edit', $item->id) }}"
                                            class="btn btn-sm ml-3 btn-secondary" title="Edit"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <form onsubmit="return confirm('Are you sure to delete {{ $item->title }}?')"
                                            action="{{ route('admin.gallery.destroy', $item->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm ml-3 btn-danger" title="Delete"><i
                                                    class="fas fa-trash"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
                <!-- Modal -->
                @foreach ($data as $item)
                    <div class="modal fade" id="galleryModal{{$item->id}}" tabindex="-1" role="dialog"
                        aria-labelledby="galleryModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="galleryModalLabel">{{ $item->title }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="{{ asset('media/images/' . $item->image) }}" alt="{{ $item->title }}" class="img-fluid">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/admin/vendors/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('assets/admin/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendors/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $('#data-table').DataTable();
    </script>
    <script>
        $('#trigger-loading-1').on('click', function(e) {
            $(this).addClass("is-loading");
            setTimeout(function() {
                $("#trigger-loading-1").removeClass("is-loading");
            }, 4000);
            // e.preventDefault();
        });
    </script>
@endsection
