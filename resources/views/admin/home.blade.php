@extends('layouts.admin.master')

@section('title-bar', 'Home')

@section('breadcrumb')
    <span class="breadcrumb-item active"><i class="anticon anticon-home m-r-5"></i>Home</span>
@endsection
