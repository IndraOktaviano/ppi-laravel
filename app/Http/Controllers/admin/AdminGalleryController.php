<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;

use Validator;
use File;

class AdminGalleryController extends Controller
{
    public function index()
    {
        $data = Gallery::all();
        return view('admin.gallery.index', compact('data'));
    }

    public function create()
    {
        return view('admin.gallery.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $name = 'gallery_' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('media/images'), $name);
        };

        $data = Gallery::create([
            'title' => $request->title,
            'image' => $name,
        ]);

        if ($data) {
            return redirect()->route('admin.gallery.index')->with('message', 'New gallery has been created');
        } else {
            return back()->with('message', 'Gallery not added');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Gallery::findOrFail($id);
        return view('admin.gallery.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        $data = Gallery::findOrFail($id);

        if ($request->hasFile('image') == '' ) {
            $data->update([
                'title' => $request->title,
            ]);
        } else {
            // hapus file lama
            $path = public_path("media/images/").$data->image;
            if (File::exists($path)) {
                unlink($path);
            }

            // tambah file baru
            $image = $request->file('image');
            $name = 'gallery_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('media/images'), $name);

            $data->update([
                'title' => $request->title,
                'image' => $name,
            ]);
        }
        if ($data) {
            return redirect()->route('admin.gallery.index')->with('message', 'New gallery has been updated');
        } else {
            return back()->with('message', 'Gallery not added');
        }
    }

    public function destroy($id)
    {
        $data = Gallery::findOrFail($id);
        $path = public_path("media/images/").$data->image;
        if (File::exists($path)) {
            unlink($path);
        }
        $data->delete();
        return back()->with('message', 'Gallery Deleted Successfully');
    }
}
