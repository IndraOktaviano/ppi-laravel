<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

use Validator;
use File;

class AdminBlogController extends Controller
{
    public function index()
    {
        $data = Blog::all();
        return view('admin.blog.index', compact('data'));
    }

    public function create()
    {
        return view('admin.blog.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'desc' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $name = 'article_' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('media/images'), $name);
        };

        if($request->hasFile('archive')){
            $file = $request->file('archive');
            $archive = 'archive_' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('media/archives'), $archive);
        };

        if ($request->hasFile('archive') == true) {
            $data = Blog::create([
                'title' => $request->title,
                'desc' => $request->desc,
                'image' => $name,
                'archive' => $archive,
                'slug' => strtolower(str_replace(' ', '-', $request->title))
            ]);
        } else {
            $data = Blog::create([
                'title' => $request->title,
                'desc' => $request->desc,
                'image' => $name,
                'slug' => strtolower(str_replace(' ', '-', $request->title))
            ]);
        };

        Blog::findOrFail($data->id)->update(['slug' => $data->slug.'-'.$data->id]);

        if ($data) {
            return redirect()->route('admin.blog.index')->with('message', 'New article has been created');
        } else {
            return back()->with('message', 'Article not added');
        }
    }

    public function show($blog)
    {
        $data = Blog::findOrFail($blog);
        return view('admin.blog.show', compact('data'));
    }

    public function edit(Blog $blog)
    {
        $data = $blog;
        return view('admin.blog.edit', compact('data'));
    }

    public function update(Request $request, $blog)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'desc' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        $data = Blog::findOrFail($blog);

        if ($request->hasFile('image') == true ) {
            // hapus file lama
            $path = public_path("media/images/").$data->image;
            if (File::exists($path)) {
                unlink($path);
            }

            // tambah file baru
            $image = $request->file('image');
            $name = 'article_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('media/images'), $name);

            $data->update([
                'image' => $name,
            ]);
        };

        if ($request->hasFile('archive') == true) {
            // hapus file lama
            if ($data->archive == true) {
                $path = public_path("media/archives/").$data->archive;
                if (File::exists($path)) {
                    unlink($path);
                }
            }

            // tambah file baru
            $file = $request->file('archive');
            $archive = 'archive_' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('media/archives'), $archive);

            $data->update([
                'archive' => $archive,
            ]);
        }

        $data->update([
            'title' => $request->title,
            'desc' => $request->desc,
            'slug' => strtolower(str_replace(' ', '-', $request->title))
        ]);
        Blog::findOrFail($data->id)->update(['slug' => $data->slug.'-'.$data->id]);

        if ($data) {
            return redirect()->route('admin.blog.index')->with('message', 'Article Updated Successfully');
        } else {
            return back()->with('message', 'Article cannot be update');
        }
    }

    public function destroy($blog)
    {
        $data = Blog::findOrFail($blog);
        $path = public_path("media/images/").$data->image;
        $archive = public_path("media/archives/").$data->archive;
        if (File::exists($path)) {
            unlink($path);
        }
        if ($data->archive == true) {
            if (File::exists($archive)) {
                unlink($archive);
            }
        }
        $data->delete();
        return back()->with('message', 'Article Deleted Successfully');
    }
}
