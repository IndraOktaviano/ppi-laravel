<?php

use App\Http\Controllers\admin\AdminBlogController;
use App\Http\Controllers\admin\AdminGalleryController;
use App\Http\Controllers\admin\AdminHomeController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomepageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->group(function () {
    Route::resource('/', HomepageController::class);
    Route::resource('blog', BlogController::class);
    Route::resource('gallery', GalleryController::class);
});

Route::middleware(['auth'])->prefix('admin')->as('admin.')->group(function () {
    Route::resource('/', AdminHomeController::class);
    Route::resource('blog', AdminBlogController::class);
    Route::resource('gallery', AdminGalleryController::class);
});

Auth::routes(['register' => false]);

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
